#include <setup/setup.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/photo/photo.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <string>

namespace ipr
{
    class imageSetup::impl
    {
    public:
        impl(const std::string &path);
	    ~impl();
        void remap(cv::Mat &img1, cv::Mat &img2);
	    void filter(const float p1,
                    const float p2,
                    const float p3);
        cv::Mat getOut1();
        cv::Mat getOut2();
    private:
        cv::Mat cam1Map1;
        cv::Mat cam1Map2;
        cv::Mat cam2Map1;
        cv::Mat cam2Map2;
        cv::Mat out1;
        cv::Mat out2;
    };
}

// Implementation

ipr::imageSetup::impl::impl(const std::string &path)
{
    cv::Mat cam1Matrix, cam2Matrix, cam1Dist, cam2Dist;
    cv::Size imgSize;
    cv::FileStorage fs;
    fs.open(path, cv::FileStorage::READ);
    fs["Image_Size"]     >> imgSize;
    fs["Camera1_Matrix"] >> cam1Matrix;
    fs["Camera1_Dist"]   >> cam1Dist;
    fs["Camera2_Matrix"] >> cam2Matrix;
    fs["Camera2_Dist"]   >> cam2Dist;
    out1 = cv::Mat::zeros(imgSize, CV_8UC3);
    out2 = cv::Mat::zeros(imgSize, CV_8UC3);
    //Camera 1 geometrical map setup
    cv::initUndistortRectifyMap(cam1Matrix, cam1Dist, cv::Mat(),
    cv::getOptimalNewCameraMatrix(cam1Matrix, cam1Dist, imgSize, 0, imgSize, 0), imgSize, CV_16SC2, cam1Map1, cam1Map2);
    //Camera 2 geometrical map setup
    cv::initUndistortRectifyMap(cam2Matrix, cam2Dist, cv::Mat(),
    cv::getOptimalNewCameraMatrix(cam2Matrix, cam2Dist, imgSize, 0, imgSize, 0), imgSize, CV_16SC2, cam2Map1, cam2Map2);
}

void ipr::imageSetup::impl::remap(cv::Mat &img1, cv::Mat &img2)
{
    cv::remap(img1, out1, cam1Map1, cam1Map2, CV_INTER_LINEAR);
    cv::remap(img2, out2, cam2Map1, cam2Map2, CV_INTER_LINEAR);
}

void ipr::imageSetup::impl::filter(const float p1,
                                   const float p2,
                                   const float p3)
{
    cv::fastNlMeansDenoisingColored(out1, out1, p1, 3, p2, p3);
    cv::fastNlMeansDenoisingColored(out2, out2, p1, 3, p2, p3);
}

cv::Mat ipr::imageSetup::impl::getOut1()
{
    return out1;
}

cv::Mat ipr::imageSetup::impl::getOut2()
{
    return out2;
}

ipr::imageSetup::impl::~impl()
{
}

// Public API

ipr::imageSetup::imageSetup(const std::string &path)
    : m_impl(new ipr::imageSetup::impl(path))
{
}

ipr::imageSetup::~imageSetup()
{
}

void ipr::imageSetup::remap(cv::Mat &img1, cv::Mat &img2)
{
    return m_impl->remap(img1, img2);
}

void ipr::imageSetup::filter(const float p1,
                             const float p2,
                             const float p3)
{
    return m_impl->filter(p1, p2, p3);
}

cv::Mat ipr::imageSetup::getOut1()
{
    return m_impl->getOut1();
}

cv::Mat ipr::imageSetup::getOut2()
{
    return m_impl->getOut2();
}
