#include <setup/setup.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <iostream>
#include <string>

int main(int argc, char **argv)
{
    cv::Mat img1, img2;
    ipr::imageSetup s(argv[1]);
    img1 = cv::imread(argv[2], CV_LOAD_IMAGE_COLOR);
    img2 = cv::imread(argv[3], CV_LOAD_IMAGE_COLOR);
    std::cout << "Remaping..." << std::endl;
    s.remap(img1, img2);
    std::cout << "Filtering..." << std::endl;
    s.filter(50, 3, 5);
    cv::imwrite("out1.png", s.getOut1());
    cv::imwrite("out2.png", s.getOut2());
    return 0;
}
