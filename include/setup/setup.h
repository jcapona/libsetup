#ifndef IPROC_STEREO_H
#define IPROC_STEREO_H

#include <string>
#include <memory>

namespace cv
{
    class Mat;
}

namespace ipr
{
    class imageSetup
    {
    public:
        imageSetup(const std::string &path);
	~imageSetup();
        void remap(cv::Mat &img1, cv::Mat &img2);
	void filter(const float p1,
                const float p2,
                const float p3);
        cv::Mat getOut1();
        cv::Mat getOut2();
    private:
	class impl;
	std::unique_ptr<impl> m_impl;
    };
}

#endif
